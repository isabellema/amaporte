Windows 11 Famille, Ubuntu 20.04 :
```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install ruby-full build-essential zlib1g-dev
mkdir gems
echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc
echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc
echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc
gem install jekyll bundler
cd amaporte
bundle update --bundler
bundle
bundle exec jekyll build
bundle exec jekyll serve
```