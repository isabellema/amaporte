---
name: chou_blanc
title: Chou blanc
---
Saison : Hiver

Recette salées

[Mijoté de Chou vert et Lentilles](https://www.marmiton.org/recettes/recette_mijote-de-chou-vert-et-lentilles_310389.aspx)

[Chou farci au camembert]([Recette chou farci au camembert (végétarien) - Marie Claire](https://www.marieclaire.fr/cuisine/chou-farci-a-la-normande,1196693.asp))

[Chou blanc raisins secs, gingembre, cardamome](http://kmillesaveurs.canalblog.com/archives/2017/01/10/34788163.html)

[Boeuf sauté au chou blanc](https://www.topsante.com/nutrition-et-recettes/recettes-sante/baeuf-saute-au-chou-blanc-8494)


Recettes sucré-salé

[Chou blanc, carottes caramélisées au soja](https://www.lavieclaire.com/recettes/curry-de-chou-kale-et-pommes-de-terre-aux-noix-de-cajou/)

