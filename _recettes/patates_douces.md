---
name: patate_douce
title: Patate douce
---
[Patates douces sautées aux oignons, à l'ail et au thym](https://cuisine.journaldesfemmes.fr/recette/1017697-poelee-de-patates-douces-sautees-aux-oignons-a-l-ail-et-au-thym)

[Purée de patate douce, pommes de terre et carottes](https://www.cuisineaz.com/recettes/puree-de-patate-douce-pommes-de-terre-et-carottes-76962.aspx)

[Frites de patates douces au four](https://www.papillesetpupilles.fr/2019/03/frites-de-patates-douces-au-four.html/)

[Crumble patate douce](https://www.cuisineactuelle.fr/recettes/crumble-patate-douce-330717)

[Gratin de patates douces](https://cuisine.journaldesfemmes.fr/recette/341943-gratin-de-patates-douces)

