---
name: panais
title: Panais
---
Saison : Automne

[Velouté de potimarron et panais](https://www.marmiton.org/recettes/recette_veloute-de-potimarron-et-panais_230523.aspx)

[Purée de panais et mimolette](https://www.750g.com/puree-de-panais-et-mimolette-r68303.htm)
